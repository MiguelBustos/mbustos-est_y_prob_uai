########################################
#### MIGUEL BUSTOS V - MII UAI 2018 ####
####        26 - 05 - 2018          ####
########################################

# Estadistica descriptiva
# Problema de tirar 3 monedas al azar
# en el archivo excel se tienen los casos reales

# Cargar libreria
library(fAsianOptions)
library(prob)

# Abrir archivo que contiene la data
MONEDA<-read.csv(file.choose(),header = TRUE,sep=";")
summary(MONEDA)

# Dimension de la matriz de datos
dim(MONEDA)

# Calculo de probabilidades
Prob(probspace(MONEDA),A=="0" & B=="0" & C=="0") # y
Prob(probspace(MONEDA),A=="1" | B=="1") # o

# Probabilidad condicional
Prob(probspace(MONEDA),A=="1",given = B=="1")

