########################################
#### MIGUEL BUSTOS V - MII UAI 2018 ####
####        07 - 06 - 2018          ####
########################################

"
################### Arbol de decision ##################
## Classification using decision trees (iris data set)##
########################################################


#Problema Validado en Clases el dia 07/06/2018

"


#resuemn
summary(iris)

#distribucion de las variables por clase
iris %>%
  gather(variable, value, -Species) %>%
  ggplot(aes(y = as.factor(variable), 
             fill = as.factor(Species), 
             x = percent_rank(value))) +
  geom_density_ridges()

# Para generar un aleatorio y otros tengan los mismos resultados
set.seed(1234)

#runif(3,0,1)

#Generate training and testing set
sub <- sample(nrow(iris), floor(nrow(iris) * 0.7))
train.set<-iris[sub, ] #70 % for training
test.set<-iris[-sub, ] #30 % for testing

#Arbol de decision se encuentra en la libreria rpart

library(rpart)

# Estructura similar al NB
# pero ahora se utiliza rpart
# Utiliza el indice GINI para hacer los test

rpart.tree <- rpart(Species ~ ., data=train.set)
plot(rpart.tree,margin=0.2)
text (rpart.tree, use.n = T, pretty = TRUE)
title("Training Set's Classification Tree")

# Importante: Como se puede ver del grafico, el largo del petalo de setosa es fundamental para hacer caer
# si el largo del petalo es mas grande entro a la derecha.
# operador Y logico y concatena.



# Para predecir en funcion al conjunto de entrenamiento
# prediction using test set
predictions <- predict(rpart.tree, test.set, type="class")
predictions
#Compare with real labels through the confusion matrix
confusionMatrix(predictions,test.set$Species)


#Prune (probar con cp=0.1, cp=0.5, cp=1)
prune.rpart.tree <- prune(rpart.tree, cp=0.1)
plot(prune.rpart.tree,margin=0.2)
text (prune.rpart.tree, use.n = T, pretty = TRUE)

#control parameters (minimum number for split)
rpart.tree <- rpart(Species ~ ., data=train.set,control = rpart.control(minsplit=5))
plot(rpart.tree,margin=0.2)
text (rpart.tree, use.n = T, pretty = TRUE)









###################################################
### prediccion usando datos de la encuesta Adult
###################################################
#Train:
m <- rpart(V15~.,data=Adult)



pdf(file="/Users/gonzaloruz/Desktop/trees.pdf",paper="letter",width=900)
plot(m,margin=0.2,main = "Classification Tree for Adult dataset")
text(m, use.n=T,pretty=TRUE)
dev.off()


plot(m,margin=0.2)
text(m, use.n=T,pretty=TRUE)
post(m, file = "/Users/gonzaloruz/Desktop/trees2.ps",
     title = "Classification Tree for Adult dataset")

#revisar el archivo prp.pdf subido a webcursos para visualizaciones de trees mas bonitas

# Test:
p<-predict(m, AdultTest,type="class")
confusionMatrix(p,AdultTest[,15])

#prune
prune.m <- prune(m, cp=0.1)
plot(prune.m,margin=0.2)
text(prune.m, use.n=T,pretty=TRUE)

post(prune.m, file = "/Users/gonzaloruz/Desktop/tree2p.ps",
     title = "Classification Tree for Adult dataset")

p<-predict(prune.m, AdultTest,type="class")
confusionMatrix(p,AdultTest[,15])

