########################################
#### MIGUEL BUSTOS V - MII UAI 2018 ####
####        26 - 05 - 2018          ####
########################################

# Estadistica descriptiva
# Problema N°1 Del curso de ESTADÍSTICA Y ANÁLISIS DE DATOS Sec.1 STGO MII 2018
# Se busca la prob. dado 

#cargar libreria
library(fAsianOptions)
library(prob)

# Abrir archivo que contiene la data
# DATASET : 1_CLASE_A-DATASET_CAR.data
CAR<-read.table(file.choose(),header = TRUE,sep=",")
#W<-read.csv(file.choose(),header = TRUE,sep=";")

# Variables:
# poner nombres a campos
# names(CAR)<-c("x1","x2","x3","x4","x5","x6","x7")
# buying      Comprado
# maint       Mantenimiento
# doors       Puertas
# persons     Personas
# lug boot    Maletero
# safety      Seguridad
# car_accept  Aceptado
# de debe revisar que significa cada variable

summary(CAR)

#dimension de la matriz de datos
dim(CAR)

#Calculo de probabilidades
Prob(probspace(CAR),doors=="2" | doors=="3" | doors=="4")

Prob(probspace(CAR),buying=="low" & maint=="low") # y

Prob(probspace(CAR),buying=="low" | maint=="low") # o

#Probabilidad condicional

Prob(probspace(CAR),car_accept=="acc",given = safety=="low")
Prob(probspace(CAR),buying=="low",given = doors=="4")

