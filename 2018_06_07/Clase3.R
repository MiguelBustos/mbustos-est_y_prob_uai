
###################################################
### Data: IPC de Australia entre el 2008 y el 
### 2010 (trimestral), CPI en ingles
###################################################
year <- rep(2008:2010, each=4)
quarter <- rep(1:4, 3)
cpi <- c(162.2, 164.6, 166.5, 166.0, 
         166.2, 167.0, 168.6, 169.5, 
         171.0, 172.1, 173.3, 174.0)
plot(cpi, xaxt="n", ylab="CPI", xlab="")
# dibujar el eje x
axis(1, labels=paste(year,quarter,sep="Q"), at=1:12, las=3)


###################################################
### revisar correlacion entre el IPC y las otras 
### variables, year y trimestre
###################################################
cor(year,cpi)
cor(quarter,cpi)


###################################################
### podemos construir un modelo de regresion lineal 
### usando year y trimestre como predictoras y CPI 
### como respuesta
###################################################
fit <- lm(cpi ~ year + quarter)
fit


###################################################
### Entonces para predecir el IPC durante el 2011
###################################################
(cpi2011 <- fit$coefficients[[1]] + fit$coefficients[[2]]*2011 +
            fit$coefficients[[3]]*(1:4))


###################################################
### mas informacion del modelo puede ser obtenida
###################################################
attributes(fit)
fit$coefficients


###################################################
### Residuos y resumen del modelo
###################################################
# Diferencias entre los valores observados y los ajustados
residuals(fit)

#Resumen del modelo
summary(fit)

###################################################
### Tambien se puede hacer una prediccion para 
### el 2011 usando la funcion predict()
###################################################
data2011 <- data.frame(year=2011, quarter=1:4)
cpi2011 <- predict(fit, newdata=data2011)
style <- c(rep(1,12), rep(2,4))
plot(c(cpi, cpi2011), xaxt="n", ylab="CPI", xlab="", pch=style, col=style)
axis(1, at=1:16, las=3,
     labels=c(paste(year,quarter,sep="Q"), "2011Q1", "2011Q2", "2011Q3", "2011Q4"))

###################################################
### Regresion logistica usando los datos Adult
###################################################
Adult<-read.csv(file.choose(),header = FALSE,sep=";")
summary(Adult)
rl<-glm(V15~V1 + V3 + V5 + V13, family = binomial("logit"), data =Adult)
summary(rl)

# prediccion usando conjunto de prueba
AdultTest<-read.csv(file.choose(),header = FALSE,sep=";")
predTest<-predict(rl,newdata=AdultTest[,-15],type="response")
predTest<-as.factor(ifelse(round(predTest)==0,"<=50K",">50K"))
confusionMatrix(predTest,AdultTest$V15)

###################################################
### GLM usando datos bodyfat
###################################################
library(TH.data)
data("bodyfat", package = "TH.data")
myFormula <- DEXfat ~ age + waistcirc + hipcirc + elbowbreadth + kneebreadth
bodyfat.glm <- glm(myFormula, family = gaussian("log"), data = bodyfat)
summary(bodyfat.glm)
pred <- predict(bodyfat.glm, type="response")


###################################################
### visualizacion
###################################################
plot(bodyfat$DEXfat, pred, xlab="Observed Values", ylab="Predicted Values")
abline(a=0, b=1)

###################################################
### rpart usando datos bodyfat
###################################################
arbol<-rpart(myFormula,data=bodyfat)
plot(arbol,margin=0.2)
text (arbol, use.n = T, pretty = TRUE)
title("Training Set's Regression Tree")

#pueden hacer predicciones
predictions <- predict(arbol, bodyfat)

# Root Mean Square Error (RMSE)
RMSE.arbol = (sum((bodyfat$DEXfat - predictions)^2) / nrow(bodyfat)) ^ 0.5
RMSE.arbol

##### mas ejemplos ####################
##### regresion lineal con datos swiss #########
library(datasets)
help(swiss)
model = lm(Fertility ~ .,data = swiss)
lm_coeff = model$coefficients
lm_coeff
summary(model)
# 70% de la variablidad (varianza) en la tasa de Fertility
# puede ser explicada via regresion lineal.

##### regresion polinomial con datos poly #########
# estimacion de los precios de las casas dado su area
data = read.csv(file.choose(),header =TRUE,sep=",")
x = data$Area 
y = data$Price

model1 = lm(y ~x) #regresion lineal 
model1$coeff  #coeficientes
model1$fit    #evalauci??n de x en el modelo

#polinomial
new_x = cbind(x,x^2) # generamos x^2
new_x
model2 = lm(y~new_x) 
model2$coeff
model2$fit

#visualizacion
library(ggplot2) 
ggplot(data = data) + geom_point(aes(x = Area,y = Price)) + 
  geom_line(aes(x = Area,y = model1$fit),color = "red") + 
  geom_line(aes(x = Area,y = model2$fit),color = "blue") + 
  theme(panel.background = element_blank())
